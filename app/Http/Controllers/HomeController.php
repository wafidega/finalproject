<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use PDF;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('admin');
    }

    public function siswa()
    {
        $siswa = DB::table('siswa')->get();
        return view('admin.siswa', compact('siswa'));
    }

    public function createsiswa()
    {
        return view('admin.createsiswa');
    }

    public function storesiswa(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'kelas' => 'required'
        ]);

        $query = DB::table('siswa')->insert([
            "nama" => $request["nama"],
            "kelas" => $request["kelas"]
        ]);

        return redirect('home/siswa');
    }

    public function editsiswa($id)
    {
        $siswa = DB::table('siswa')->where('id', $id)->first();
        return view('admin.editsiswa', compact('siswa'));
    }

    public function updatesiswa($id, Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'kelas' => 'required'
        ]);

        $query = DB::table('siswa')
            ->where('id', $id)
            ->update([
                'nama' => $request["nama"],
                'kelas' => $request["kelas"]
            ]);

        return redirect('home/siswa');
    }

    public function destroysiswa($id)
    {
        $query = DB::table('siswa')->where('id', $id)->delete();
        return redirect('/home/siswa');
    }

    public function guru()
    {
        $guru = DB::table('guru')->get();
        return view('admin.guru', compact('guru'));
    }

    public function createguru()
    {
        return view('admin.createguru');
    }

    public function storeguru(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'mengajar' => 'required',
            'alamat' => 'required'
        ]);

        $query = DB::table('guru')->insert([
            "nama" => $request["nama"],
            "mengajar" => $request["mengajar"],
            "alamat" => $request["alamat"]
        ]);

        return redirect('home/guru');
    }

    public function editguru($id)
    {
        $guru = DB::table('guru')->where('id', $id)->first();
        return view('admin.editguru', compact('guru'));
    }

    public function updateguru($id, Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'mengajar' => 'required',
            'alamat' => 'required'
        ]);

        $query = DB::table('guru')
            ->where('id', $id)
            ->update([
                "nama" => $request["nama"],
                "mengajar" => $request["mengajar"],
                "alamat" => $request["alamat"]
            ]);

        return redirect('home/guru');
    }

    public function destroyguru($id)
    {
        $query = DB::table('guru')->where('id', $id)->delete();
        return redirect('/home/guru');
    }

    public function jadwal()
    {
        $jadwal = DB::table('jadwal')->get();
        return view('admin.jadwal', compact('jadwal'));
    }
    public function createjadwal()
    {
        return view('admin.createjadwal');
    }
    public function storejadwal(Request $request)
    {
        $request->validate([
            'jam' => 'required',
            'matpel' => 'required'
        ]);

        $query = DB::table('jadwal')->insert([
            "jam" => $request["jam"],
            "matpel" => $request["matpel"]
        ]);

        return redirect('home/jadwal');
    }

    public function editjadwal($id)
    {
        $jadwal = DB::table('jadwal')->where('id', $id)->first();
        return view('admin.editjadwal', compact('jadwal'));
    }

    public function updatejadwal($id, Request $request)
    {
        $request->validate([
            'jam' => 'required',
            'matpel' => 'required'
        ]);

        $query = DB::table('jadwal')
            ->where('id', $id)
            ->update([
                "jam" => $request["jam"],
                "matpel" => $request["matpel"]
            ]);

        return redirect('home/jadwal');
    }

    public function destroyjadwal($id)
    {
        $query = DB::table('jadwal')->where('id', $id)->delete();
        return redirect('/home/jadwal');
    }

    public function cetak()
    {
        $pdf = PDF::loadview('admin.peraturan');
        return $pdf->stream('test.pdf');
    }
}
