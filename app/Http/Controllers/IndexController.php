<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class IndexController extends Controller
{
    public function siswa() {
    	$siswa = DB::table('siswa')->get();
        return view('pages.siswa', compact('siswa'));
    }
    public function guru() {
    	$guru = DB::table('guru')->get();
        return view('pages.guru', compact('guru'));
    }
    public function jadwal() {
    	$jadwal = DB::table('jadwal')->get();
        return view('pages.jadwal', compact('jadwal'));
    }
}
