<?php

use Illuminate\Database\Seeder;

class GuruSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $user = new \App\User;
		 $user->name = "Wafi Pandega";
		 $user->email = "wpandega81@gmail.com";
		 $user->password = \Hash::make("pandega");
		 $user->save();
		 $this->command->info("User Admin berhasil dibuat");
    }
}
