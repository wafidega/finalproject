<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});
Route::get('/siswa', 'IndexController@siswa');
Route::get('/guru', 'IndexController@guru');
Route::get('/jadwal', 'IndexController@jadwal');





Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/home/siswa', 'HomeController@siswa');
Route::get('/home/siswa/createsiswa', 'HomeController@createsiswa');
Route::post('/home/siswa', 'HomeController@storesiswa');
Route::get('/home/siswa/{siswa_id}/edit', 'HomeController@editsiswa');
Route::put('/home/siswa/{siswa_id}', 'HomeController@updatesiswa');
Route::delete('/home/siswa/{siswa_id}', 'HomeController@destroysiswa');

Route::get('/home/guru', 'HomeController@guru');
Route::get('/home/guru/createguru', 'HomeController@createguru');
Route::post('/home/guru', 'HomeController@storeguru');
Route::get('/home/guru/{guru_id}/edit', 'HomeController@editguru');
Route::put('/home/guru/{guru_id}', 'HomeController@updateguru');
Route::delete('/home/guru/{guru_id}', 'HomeController@destroyguru');

Route::get('/home/jadwal', 'HomeController@jadwal');
Route::get('/home/jadwal/createjadwal', 'HomeController@createjadwal');
Route::post('/home/jadwal', 'HomeController@storejadwal');
Route::get('/home/jadwal/{jadwal_id}/edit', 'HomeController@editjadwal');
Route::put('/home/jadwal/{jadwal_id}', 'HomeController@updatejadwal');
Route::delete('/home/jadwal/{jadwal_id}', 'HomeController@destroyjadwal');

Route::get('/home/print', 'HomeController@cetak');
