@extends('include.content')

@section('breadcrum')
	<!-- ======= Breadcrumbs ======= -->
    <div class="breadcrumbs" data-aos="fade-in" style="margin-bottom: 20px">
      <div class="container">
        <h2>Jadwal Pelajaran Hari ini</h2>
        <h3>note : Jadwal akan diupdate setiam jam 19:00</h3>
      </div>
    </div><!-- End Breadcrumbs -->
    <div class="container" data-aos="fade-up">
        <div class="box">
          <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Jam</th>
                      <th>Mata Pelajaran</th>
                    </tr>
                  </thead>
                  <tbody>
                        @foreach($jadwal as $key => $post)
                        <tr>
                          <td> {{$key + 1}} </td>
                          <td> {{$post->jam}} </td>
                          <td> {{$post->matpel}} </td>
                        </tr>
                      @endforeach
                  </tbody>
                </table>
        </div>
        </div>
@endsection
@section('content')
			
@endsection