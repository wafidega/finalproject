@extends('include.content')

@section('breadcrum')
	<!-- ======= Breadcrumbs ======= -->
    <div class="breadcrumbs" data-aos="fade-in">
      <div class="container">
        <h2>Login</h2>
        <p>Halaman ini khusus untuk guru, untuk informasi lebih lanjut hubungi:</p>
        <h3>adminsia@gmail.com</h3>
      </div>
    </div><!-- End Breadcrumbs -->
@endsection
@section('content')
			<form action="{{route('login')}}" method="post">
				@csrf
              <input type="text" id="login" class="fadeIn second" name="email" placeholder="email">
              <input type="password" id="password" class="fadeIn third" name="password" placeholder="password">
              <input type="submit" class="fadeIn fourth" value="Log In">
            </form>
@endsection