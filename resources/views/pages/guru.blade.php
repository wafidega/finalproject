@extends('include.content')

@section('breadcrum')
	<!-- ======= Breadcrumbs ======= -->
    <div class="breadcrumbs" data-aos="fade-in" style="margin-bottom: 20px">
      <div class="container">
        <h2>Tabel Guru</h2>
      </div>
    </div><!-- End Breadcrumbs -->
    <div class="container" data-aos="fade-up">
        <div class="box">
          <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Nama</th>
                      <th>Mengajar</th>
                      <th>Alamat</th>
                    </tr>
                  </thead>
                  <tbody>
                        @foreach($guru as $key => $post)
                        <tr>
                          <td> {{$key + 1}} </td>
                          <td> {{$post->nama}} </td>
                          <td> {{$post->mengajar}} </td>
                          <td> {{$post->alamat}} </td>
                        </tr>
                      @endforeach
          
                  </tbody>
                </table>
        </div>
        </div>
@endsection
@section('content')
			
@endsection