<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Sistem Informasi Sekolah</title>
  <meta content="" name="description">
  <meta content="" name="keywords">



  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="{{asset('/admin/assets/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{asset('/admin/assets/vendor/icofont/icofont.min.css')}}" rel="stylesheet">
  <link href="{{asset('/admin/assets/vendor/boxicons/css/boxicons.min.css')}}" rel="stylesheet">
  <link href="{{asset('/admin/assets/vendor/remixicon/remixicon.css')}}" rel="stylesheet">
  <link href="{{asset('/admin/assets/vendor/owl.carousel/assets/owl.carousel.min.css')}}" rel="stylesheet">
  <link href="{{asset('/admin/assets/vendor/animate.css/animate.min.css')}}" rel="stylesheet">
  <link href="{{asset('/admin/assets/vendor/aos/aos.css')}}" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="{{asset('/admin/assets/css/style.css')}}" rel="stylesheet">

  <!-- =======================================================
  * Template Name: Mentor - v2.2.0
  * Template URL: https://bootstrapmade.com/mentor-free-education-bootstrap-theme/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top">
    <div class="container d-flex align-items-center">

      <h1 class="logo mr-auto"><a href="/home" style="color: #3498db">Admin Guru</a></h1>
      <!-- Uncomment below if you prefer to use an image logo -->
      <!-- <a href="index.html" class="logo mr-auto"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->

      <nav class="nav-menu d-none d-lg-block">
        <ul>
          <li><a href="/home">Home</a></li>
          <li><a href="/home/guru">Data Guru</a></li>
        </ul>
      </nav><!-- .nav-menu -->
    </div>
  </header><!-- End Header -->

  <main id="main">
    <!-- ======= Breadcrumbs ======= -->
    <div class="breadcrumbs" data-aos="fade-in">
      <div class="container">
        <h2>Data Guru ke-{{$guru->id}} </h2>
      </div>
    </div><!-- End Breadcrumbs -->

    <!-- ======= Pricing Section ======= -->
    <section id="pricing" class="pricing">
      <div class="container" data-aos="fade-up">
        <div class="box">
          <form action="/home/guru/{{$guru->id}}" method="POST">
            @csrf
            @method('PUT')
            <div class="card-body">
              <div class="form-group">
                <label for="nama">Nama</label>
                <input type="text" class="form-control" id="nama" placeholder="Tulis Nama" name="nama" value="{{$guru->nama}}">
                @error('nama')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
              </div>
              <div class="form-group">
                <label for="mengajar">Mengajar</label>
                <input type="text" class="form-control" id="mengajar" placeholder="Tulis Alamat" name="mengajar" value="{{$guru->mengajar}}">
                @error('mengajar')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
              </div>
              <div class="form-group">
                <label for="alamat">Alamat</label>
                <input type="text" class="form-control" id="alamat" placeholder="Tulis Matpel Mengajar" name="alamat" value="{{$guru->alamat}}">
                @error('alamat')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
              </div>
            </div>
            <!-- /.card-body -->

            <div class="card-footer">
              <button type="submit" class="btn btn-primary">Update</button>
            </div>
          </form>
        </div>
      </div>

      </div>
    </section><!-- End Pricing Section -->

  </main><!-- End #main -->





  <!-- Vendor JS Files -->
  <script src="{{asset('/admin/assets/vendor/jquery/jquery.min.js')}}"></script>
  <script src="{{asset('/admin/assets/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
  <script src="{{asset('/admin/assets/vendor/jquery.easing/jquery.easing.min.js')}}"></script>
  <script src="{{asset('/admin/assets/vendor/php-email-form/validate.js')}}"></script>
  <script src="{{asset('/admin/assets/vendor/waypoints/jquery.waypoints.min.js')}}"></script>
  <script src="{{asset('/admin/assets/vendor/counterup/counterup.min.js')}}"></script>
  <script src="{{asset('/admin/assets/vendor/owl.carousel/owl.carousel.min.js')}}"></script>
  <script src="{{asset('/admin/assets/vendor/aos/aos.js')}}"></script>

  <!-- Template Main JS File -->
  <script src="{{asset('/admin/assets/js/main.js')}}"></script>

</body>

</html>